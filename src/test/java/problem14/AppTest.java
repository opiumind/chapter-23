package problem14;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    public void testSort() throws Exception {
        App.sort("testlargedata.dat", "testsortedfile.dat");
        assertEquals("Wrong sorting.", "127 275 394 1220 1539 1620 1714 1722 1846 2087 ", App.displayFile("testsortedfile.dat", 10));
    }

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
}
